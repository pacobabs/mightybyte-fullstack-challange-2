const express = require("express");
const requestIp = require("request-ip");
const fs = require("fs");
const path = require("path");

const router = express.Router();

router.post("/response", async (req, res) => {
  try {
    const rawdata = fs.readFileSync(path.resolve(__dirname, "../data.json"));
    const data = JSON.parse(rawdata);
    if (data) {
      const ip = requestIp.getClientIp(req);
      const entry = Object.entries(data).find(
        ([_, { clientIp }]) => clientIp === ip
      );
      if (entry && entry[1].url) {
        return res.end(JSON.stringify({ shortenedURL: entry[1].url }));
      }
    }
    res.end();
  } catch (error) {
    console.error(error);
    res.status(500).json("Internal server error");
  }
});

module.exports = router;
