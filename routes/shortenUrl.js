const express = require("express");
const validUrl = require("valid-url");
const isValidDomain = require("is-valid-domain");
const { nanoid } = require("nanoid");
const fs = require("fs");
const path = require("path");
const requestIp = require("request-ip");

const router = express.Router();

router.post("/url", async (req, res) => {
  try {
    const { url } = req.body;

    if (validUrl.isUri(url) || isValidDomain(url)) {
      const rawdata = fs.readFileSync(path.resolve(__dirname, "../data.json"));
      const data = JSON.parse(rawdata);

      const clientIp = requestIp.getClientIp(req);

      if (data) {
        const entry = Object.entries(data).find(
          ([_, { url: existingUrl }]) => url === existingUrl
        );
        const code = entry ? entry[0] : nanoid(10);
        data[code] = { url, clientIp };
      }

      fs.writeFile("data.json", JSON.stringify(data), (err) =>
        console.log(err)
      );

      res.end("see https://localhost:3000/response");
    } else {
      res.status(401).json("Invalid url");
    }
  } catch (error) {
    console.error(error);
    res.status(500).json("Internal server error");
  }
});

module.exports = router;
