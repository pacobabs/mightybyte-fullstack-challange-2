const express = require("express");
const fs = require("fs");
const path = require("path");

const router = express.Router();

router.get("/:code", async (req, res) => {
  try {
    const rawdata = fs.readFileSync(path.resolve(__dirname, "../data.json"));
    const data = JSON.parse(rawdata);
    const { code } = req.params;

    if (data && data[code]) {
      res.json({ url: data[code].url });
    } else {
      res.status(401).json("Invalid code");
    }
  } catch (error) {
    console.error(error);
    res.status(500).json("Internal server error");
  }
});

module.exports = router;
