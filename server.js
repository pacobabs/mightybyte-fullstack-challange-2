const express = require("express");

const app = express();

app.use(
  express.json({
    extended: false,
  })
);

app.use("/", require("./routes/response"));
app.use("/", require("./routes/shortenUrl"));
app.use("/", require("./routes/getUrl"));

app.listen(3000, () => console.log("server started at PORT 3000"));
